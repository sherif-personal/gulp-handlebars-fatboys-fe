$(document).ready(function () {

  console.log("main scripts");

  //mute banner background video 
  if ($(".banner-wrapper").length) {
    $("video").prop('volume', 0.5);
    $("video").prop('muted', true); //mute
  }

  //check if element is in view 
  function isInViewport(node) {
    var rect = node.getBoundingClientRect()
    return (
      (rect.height > 0 || rect.width > 0) &&
      rect.bottom >= 0 &&
      rect.right >= 0 &&
      rect.top <= (window.innerHeight || document.documentElement.clientHeight) &&
      rect.left <= (window.innerWidth || document.documentElement.clientWidth)
    )
  }


  // ----------------------------------------------------------------------
  // att: mobile image   / equale size tile
  // ----------------------------------------------------------------------
  (function () {
    "use strict";

    // var mobileWidth = window.matchMedia("(max-width: 768px)").matches;

    var mobileWidth = window.matchMedia("(max-width: 820px)").matches;
    var target = $('.mobile-image-enabled');
    for (var i = 0, len = target.length; i < len; i++) {

      var src = $(target[i]).data('img-mobile');

      if (mobileWidth && src.replace(/\s/g, "") != "") {
        $(target[i]).attr('style', src);
      }
    }

    // var mobileWidth = window.matchMedia("(max-width: 768px)").matches;
    // console.log("testMobile", mobileWidth);

    var oneTileWidth = window.innerWidth;
    var twoTileWidth = window.innerWidth / 2;
    var threeTileWidth = window.innerWidth / 3;
    var fiveTileWidth = window.innerWidth / 5;


    if (mobileWidth == true) {


      $(".animate-box-1").remove();
      $(".animate-box-2").remove();

      $(".mtrFirstTile .tile-box").trigger("click");
      // console.log("AAAA")

      //mtr tile
      $(".col-5-tile").css('height', twoTileWidth);

      //tile 2col
      $(".col-2-tile").css('height', oneTileWidth);

      //tile 3col
      $(".col-3-tile").css('height', oneTileWidth);


    } else {
      // to make tile square
      $(".col-2-tile").css('height', twoTileWidth);
      $(".col-3-tile").css('height', threeTileWidth);
      $(".col-5-tile").css('height', fiveTileWidth);

    }

    var onlyIphoneX = window.innerWidth;
    if (onlyIphoneX == 812) {
      $(".banner-wrapper").addClass("iphoneX-wrapper");
      $(".tile-main-container").addClass("iphoneX-wrapper");

    } else {}


  })();







  // ----------------------------------------------------------------------
  // slick partner carousel
  // ----------------------------------------------------------------------
  (function () {
    "use strict";

    $('.partner-logo-carousel').slick({
      slidesToShow: 1,
      dots: true,
      centerMode: true,
      dots: true,
      arrows: true,
      fade: true,

    });

    // console.log("carousel",$(".logo-slider-wrapper").length )
    // if($(".logo-slider-wrapper").length < 1){
    //   $(".partner-logo-carousel").hide();
    // }

  })();


  // ----------------------------------------------------------------------
  // scroll to section
  // ----------------------------------------------------------------------
  (function () {
    "use strict";

    scrollDownArrowFade();

    var currHeight = $("section").outerHeight();

    function scrollDownArrowFade() {
      var halfScreen = $(window).outerHeight() / 3;
      var winScroll = $(window).scrollTop();

      if (winScroll < halfScreen) {
        $(".scroll-arrow img").fadeIn(500);
      } else {
        $(".scroll-arrow img").fadeOut(500);
      }

    }


    $(window).scroll(function () {
      scrollDownArrowFade();
    });



    $(".scroll-arrow img").click(function (e) {
      e.preventDefault();
      $('html, body').animate({
        scrollTop: currHeight
      }, 1000);

    });

  })();






  // ----------------------------------------------------------------------
  // aos animation 
  // ----------------------------------------------------------------------
  (function () {
    "use strict";
    AOS.init();

    // $.scrollify({
    //   standardScrollElements : ".section",
    //   overflowScroll:"false",
    //   updateHash: false,
    //   interstitialSection : "footer, .normalScroll, section ",

    // });


    //test parallax animate  layers///

    $(window).scroll(function () {

      $('.parallax').each(function (index, element) {

        var scrolled = $(window).scrollTop()
        var initY = $(this).offset().top
        var height = $(this).height();
        var endY = initY + $(this).height()


        // Check if the element is in the viewport.

        var visible = isInViewport(this)
        if (visible) {

          // smoke layer move left right
          var diff = scrolled - initY
          var ratio = Math.round((diff / height) * 16.8 * -1)
          $(this).find(".animate-movement").attr('data-aos-delay', '0');
          $(this).find(".animate-movement").fadeIn(500).css('transform', 'translate(' + ratio + '%)');
          // smoke end


          //zoom in effect 
          var docHeight = document.documentElement.offsetHeight;
          var zoomVal = 1 + window.scrollY / Math.round((docHeight - window.innerHeight)) - 0.4;
          //console.log(zoomVal)
          $(".animate-zoom-in").css({
            // width: (60 + scroll / 50) + "%"
            transform: 'scale(' + zoomVal + ')'
          });
          $(this).find(".animate-zoom-in").attr('data-aos-delay', '0');
          //zoom end 

        } else {
          //reset delay
          // $(this).find(".animate-movement").fadeOut(500).css('right', '0px');
          $(document).find(".animate-movement .animate-zoom-in").attr('data-aos-delay', '100');
          $(this).find(".animate-movement").fadeOut(500).css('transform', 'translate(10%)');
        }
      })



    });

    //test end //



  })();





  // ----------------------------------------------------------------------
  // Form Float Label
  // ----------------------------------------------------------------------

  (function () {
    "use strict";

    $('.form-control').on('focus', function() {
      $(this).parent().addClass('is-focused');
    });


    $('.form-control').each(function () {
      if($(this).val() == '') {
        $(this).parent().removeClass('is-active');
      } else {
        $(this).parent().addClass('is-active');
        $(this).parent().addClass('is-focused');

      }
    });

    $('.form-control').on('input', function() {

      if($(this).val() == '') {
        $(this).parent().removeClass('is-active');
      } else {
        $(this).parent().addClass('is-active');
        $(this).parent().addClass('is-focused');
      }

    });
    
    $('.form-control').on('blur', function() {
      $(this).parent().removeClass('is-focused');
      
      if($(this).val() == '') {
        $(this).parent().removeClass('is-active');
      }else {
        $(this).parent().addClass('is-active');
        $(this).parent().addClass('is-focused');

      }
    });

    
  })();




  // ----------------------------------------------------------------------
  // Fade In video banner
  // ----------------------------------------------------------------------

  (function () {
    "use strict";
    $(".background-video").fadeOut(0).delay(3000).fadeIn(5000);

    
  })();



  // ----------------------------------------------------------------------
  // Video click image play 
  // ----------------------------------------------------------------------
  (function () {
    "use strict";

    $(".video-wrapper video").css("opacity", "0");
    $('.video-container').click(function () {

      // var getIframe = $(this).find(".video-wrapper").html()
      //$(this).find(".video-wrapper").html(getIframe);
      $(this).find(".video-wrapper .video").css("opacity", "1");
      //var video = $(this).attr('.video');
      //  video.play();
      $(this).find("video").click();
      //   console.log("aaaa")

    });

    
    $('.video-container').find("video").on('ended', function () {
     $(this).css("opacity", "0");
    });



  })();








});
//ready end
