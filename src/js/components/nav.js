//Nav component scripts
(function () {

  var nav = $("nav");
  var navMenuIcon = nav.find(".menu-icon-wrapper a");
  var navMenuItems = nav.find(".nav-menu-wrapper");


  if (nav.length) {

    navMenuIcon.on("click", function (event) {
      event.preventDefault();

      var self = $(this);
      self.toggleClass("menu-open");
      navMenuItems.toggleClass("menu-open");

      $(".menu-icon-wrapper a").find("img").fadeOut(0);
      if ($(".menu-open").length) {
        $(".menu-icon-wrapper a").find("img").fadeIn(400).attr("src", "/wp-content/themes/customfatboys/img/close-icon.png");
      } else {
        $(".menu-icon-wrapper a").find("img").fadeIn(400).attr("src", "/wp-content/themes/customfatboys/img/menu-icon.png");
      }

    });


  }


})();
